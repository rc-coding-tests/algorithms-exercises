// Node.js Write Stream
// In the JavaScript file, write a program to perform a GET request on the route https://coderbyte.com/api/challenges/json/age-counting which contains a data key and the value is a string which contains items in the format: key=STRING, age=INTEGER. Your goal is to count how many items exist that have an age equal to 32. Then you should create a write stream to a file called output.txt and the contents should be the key values (from the json) each on a separate line in the order they appeared in the json file (the file should end with a newline character on its own line). Finally, then output the SHA1 hash of the file.

// Example Input
// { "data": "key=IAfpK, age=32, key=WNVdi, age=64, key=jp9zt, age=40, key=9snd2, age=32" }

// Example Output
// 7caa78c7180ea52e5193d2b4c22e5e8a9e03b486

const fs = require('fs');
const axios = require('axios');
const crypto = require('crypto');

(async () => {
    const { data: { data } } = await axios.get('https://coderbyte.com/api/challenges/json/age-counting')

    const items = data.match(/(^[, ]|)key=.{5}, age=([0-9]|[^,])*/g).map(i => {
        const result = i.split(', ').reduce((accum, val) => {
            const [key, value] = val.split('=')
            accum[key] = value
            return accum
        }, {})
        return result
    })

    const itemsEqual32 = items.filter(i => i.age == '32')
    const dataToWrite = itemsEqual32.reduce((acc, i) => {
        acc += `${i.key}\n`
        return acc
    }, '')

    const writter = fs.createWriteStream('output.txt', { flags: 'a' })

    writter.write(dataToWrite, () => {
        const fileContent = fs.readFileSync('output.txt', { encoding: 'utf8', flag: 'r' })
        const hash = crypto.createHash('sha1')
        const result = hash.update(fileContent, 'utf-8')
        console.log(result.digest('hex'))
    })
})()
